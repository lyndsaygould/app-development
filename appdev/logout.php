<?php
	require_once('functions.php');
	echo makeHeader("Logout");
	
	echo"<div id=\"logout\" data-role=\"page\">
		<div data-role=\"header\"><div id=\"page-logo\"><img src=\"logo.png\" alt=\"logo\"></div></div>
				
		<div data-role=\"content\">";
	
	$_SESSION = array(); 		

	session_destroy();

	$referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'index.php';
	header( "refresh:5;url=index.php" );
	
	echo"<p>You have been successfully logged out. You will be redirected back to the login page in 5 seconds.<br />
	<a href=\"index.php\">Go back to the login page?</a></p>";
	
		echo getFooter();
?>