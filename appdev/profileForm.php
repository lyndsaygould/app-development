<?php
	require_once('functions.php');
	echo makeHeader("Upload Profile Photo");
	
	echo"
		<div id=\"user-profile-photo\" data-role=\"page\">
			<div data-role=\"header\"><div id=\"page-logo\"><img src=\"logo.png\" alt=\"logo\"></div></div>
			<div id=\"center-login\">
				
			<div data-role=\"content\">
	";

	if (isset ($_SESSION['uName'])) {
				$username = $_SESSION['uName'];
				echo"<p class=\"center\">Welcome, $username!</p>";
		
	echo"
		<form id=\"uploadPhoto\" action=\"uploadProfile.php\" method=\"post\" data-ajax=\"false\" enctype=\"multipart/form-data\"><br />
			<label for=\"profilePhoto\">Please upload a profile picture:</label> <input type=\"file\" name=\"profilePhoto\" id=\"profilePhoto\" required><br />
			<input type=\"submit\" value=\"Upload Photo\" /><br />
		</form>
		
		<p class=\"center\"><a href=\"uploadProfile.php?default=true\">Alternatively, use the default profile picture.</p>
	";
	}
	
?>

		</div><!-- end center-login-->
		</div><!-- end content-->	
	</div><!-- end page-->	