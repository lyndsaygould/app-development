<?php
	require_once('functions.php');
	echo makeHeader("View All Threads");
	
	//connect to database
	$conn = getConnection();
			  if ($conn === false) {			
				 echo "<p>Connection failed:".mysqli_connect_error()."</p>\n";		
			  }
			  
	$sql = "SELECT *
					FROM ma_thread
					JOIN ma_user
					ON ma_user.userID = ma_thread.userID
					ORDER BY ma_thread.timeStamp DESC
					";

	//perform query on the database
	$eventQuery = mysqli_query($conn, $sql) or die(mysqli_error($conn));
?>

	<div id="all-threads" data-role="page">
	<div data-role="header"><div id="page-logo"><img src="logo.png" alt="logo"></div></div>
			
	<div data-role="content">
	
<?php
	while ($row = mysqli_fetch_assoc($eventQuery)) {
		$username = $row['username'];
		$profilePhoto = $row['profilePhoto'];
		$threadText = $row['threadText'];
		$threadID = $row['threadID'];
		$threadImage = $row['threadImage'];
		$threadTitle = $row['threadTitle'];
		$faveCount = $row['faveCount'];

		echo"
		
		<div class=\"single-post\">
			<div class=\"profile-photo\">
			<img src=\"$profilePhoto\" alt=\"profile photo\">
			</div>
			<div class=\"single-post-info\">
			<h2 class=\"username\">$username</h2>
			<a href=\"viewAllSelected.php?threadID=$threadID\"><h2 class=\"title\">$threadTitle</h2></a></div>
			<div class=\"single-post-content\">
		";
		
		if ($threadText !== '') {
			echo"<p>$threadText</p>";
		}
		
		if ($threadImage !== '') {
			echo"<img src=\"$threadImage\" alt=\"thread image\">";
		}
		
		$commentCount = "SELECT *
					FROM ma_comment
					WHERE threadID = '$threadID'
					";

		//perform query on the database
		$commentQuery = mysqli_query($conn, $commentCount) or die(mysqli_error($conn));
		$commentNumber = mysqli_num_rows($commentQuery);
		
		echo"
			<div class=\"clear\"></div><p class=\"comments\">";
			
			//sort out if single/plural
			if ($faveCount == 1) {
				echo"$faveCount favourite  /  ";
			} else {
				echo"$faveCount favourites  /  ";
			}
			
			if ($commentNumber == 1) {
				echo"$commentNumber comment";
			} else {
				echo"$commentNumber comments";
			}
			
			echo"
			</p>
			<a href=\"viewAllSelected.php?threadID=$threadID\" class=\"view-all\">View all</a>
			";
			
			//if there is a session
				if (isset($_SESSION['login'])) {
					//get username
					$currentUser = $_SESSION['uName'];
					} else {
						$currentUser = null;
						}
				//if the logged in username matches the user who posted the thread	
				if ($username == $currentUser) {
					//display edit and delete links
					echo "
					<div class=\"edit-delete\"><a href=\"editThread.php?threadID=$threadID\"><i class=\"material-icons\">mode_edit</i></a>
					<a href=\"deleteThread.php?threadID=$threadID\" onClick=\"return confirm('Are you sure you want to delete this thread?')\"><i class=\"material-icons\">delete</i></a>
					</div>
					";
				} 
				echo"
			</div><!-- end content -->
			<div class=\"clear\"></div>
		</div><!--end single-post -->
		";
	}
			
	mysqli_free_result($eventQuery);	
	mysqli_close($conn);
	echo getFooter();
?>