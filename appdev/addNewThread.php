<?php
	require_once('functions.php');
	echo makeHeader("Add New Thread");
?>

	<div id="add-new-thread" data-role="page">
	<div data-role="header"><div id="page-logo"><img src="logo.png" alt="logo"></div></div>
			
	<div data-role="content">
	
<?php
	//if there is an image, upload it
	if (!empty($_FILES['imgUpload']['name'])) {
		//upload image to the ftp folder
		//set the directory the images are to be saved in
		$target_dir = "uploads/";

		//specify the path of the file that will be uploaded
		//imgUpload taken from the form
		$target_file = $target_dir . basename($_FILES["imgUpload"]["name"]);

		//default upload to 1, this means true. this will be used later. if changed to 0 the file will not upload
		$upload = 1;

		//take the extention of the file that is being uploaded
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

		//make an array for errors
		$errors = array();
		
		//check to see if file already exists in file
		if (file_exists($target_file)) {
			$errors[] = "File already exists";
			$upload = 0;
		}

		//check file size
		if ($_FILES["imgUpload"]["size"] > 1000000) {
			$errors[] = "File is too large";
			$upload = 0;
		}

		//check to see if the format extention is ok and a valid image type
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
			$errors[] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed";
			$upload = 0;
		}
		//check to see if $upload has been set false by an error
		if ($upload == 0) {
			//don't upload
		//if not, upload the file
		} else {
			if (move_uploaded_file($_FILES["imgUpload"]["tmp_name"], $target_file)) {
				//upload!
			} else { //if there is some sort of error when uploadng the file echo an error message
				// Sorry, there was an error uploading your file
			}
		}
		}

	//connect to database
	$conn = getConnection();
			  if ($conn === false) {			
				 echo "<p>Connection failed:".mysqli_connect_error()."</p>\n";		
			  }
			  
	//retrieve from form
	$userID = isset($_REQUEST['userID']) ? $_REQUEST['userID'] : null;
	$newThreadText = isset($_REQUEST['newThread']) ? $_REQUEST['newThread'] : null;
	$threadTitle = isset($_REQUEST['threadTitle']) ? $_REQUEST['threadTitle'] : null;
	
	$newThreadText = trim($newThreadText);
	$threadTitle = trim($threadTitle);
	
	$newThreadText = filter_var($newThreadText, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
	$threadTitle = filter_var($threadTitle, FILTER_SANITIZE_SPECIAL_CHARS);
	
	//the thread must have a title
		if ($threadTitle == '') {
		$errors[] = "Please enter a title";
	}
	
	//make file
	$imageName = $_FILES["imgUpload"]["name"];
	if ($imageName == '') {
		$threadImage = '';
	} else {
		$threadImage = "uploads/" . $imageName;
	}
	
	//the thread must contain text, an image OR both
	if (($imageName == '') && ($newThreadText == '')) {
		$errors[] = "Please upload either text or an image";
	}
	
	if (!empty($errors)) {
				for ($a = 0; $a < count($errors); $a++) {
					echo "$errors[$a] <br />\n";
				}
	$update = false;
	} else {

	//update database
	$updateDatabase = "INSERT INTO ma_thread (threadTitle, threadImage, threadText, userID)
					   values('$threadTitle', '$threadImage', '$newThreadText', '$userID') 
					   ";
					   
	$update = mysqli_query($conn, $updateDatabase) or die (mysqli_error($conn));
	}
	
	if ($update) {
		$getThreadID = "SELECT threadID
						FROM ma_thread
						WHERE threadTitle = '$threadTitle'
						";
						
		$threadQuery = mysqli_query($conn, $getThreadID) or die (mysqli_error($conn));
		
		while ($row = mysqli_fetch_assoc($threadQuery)) {
		$newThreadID = $row['threadID'];
		}
		//redirect to new thread
		header( "refresh:1;url=viewAllSelected.php?threadID=$newThreadID" );
		echo"You have successfully added a thread, you are now being redirected to it";
	}
	echo getFooter();
?>