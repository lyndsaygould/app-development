<?php
	require_once('functions.php');
	echo makeHeader("Upload Profile Photo");
?>

	<div id="upload-profile-photo" data-role="page">
	<div data-role="header"><div id="page-logo"><img src="logo.png" alt="logo"></div></div>
			
	<div data-role="content">
	
<?php	
	//connect to database
	$conn = getConnection();
			  if ($conn === false) {			
				 echo "<p>Connection failed:".mysqli_connect_error()."</p>\n";		
			  }
			  
	$default = isset($_REQUEST['default']) ? $_REQUEST['default'] : null;
	
	if ($default == true) {
		header( "refresh:5;url=imgUpload.php" );
		$imageName = "default.png";
		$profilePhoto = "profile/" . $imageName;
	} else {
	
	//upload profile photo
	//upload image to the ftp folder
	//set the directory the images are to be saved in
	$target_dir = "profile/";
	//specify the path of the file that will be uploaded
	//profilePhoto taken from the form
	$target_file = $target_dir . basename($_FILES["profilePhoto"]["name"]);

	//default upload to 1, this means true. this will be used later. if changed to 0 the file will not upload
	$upload = 1;

	//take the extention of the file that is being uploaded
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

	//check file size
	if ($_FILES["profilePhoto"]["size"] > 500000) {
		echo "File is too large";
		$upload = 0;
	}

	//check to see if the format extention is ok and a valid image type
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
		echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed";
		$upload = 0;
	}
	//check to see if $upload has been set false by an error
	if ($upload == 0) {
		echo "Sorry, your file was not uploaded.";
	//if not, upload the file
	} else {
		if (move_uploaded_file($_FILES["profilePhoto"]["tmp_name"], $target_file)) {
			//successful
		} else { //if there is some sort of error when uploadng the file echo an error message
			echo "Sorry, there was an error uploading your file.";
	}
	}
	
	//make file
	$imageName = $_FILES["profilePhoto"]["name"];
	$profilePhoto = "profile/" . $imageName;
	} 
	
	//use username to attach profile photo
	//get username of user logged in
	if (isset ($_SESSION['uName'])) {
			$username = $_SESSION['uName'];
	}
	
	//update database with file
	$updateDatabase = "UPDATE ma_user 
					   SET profilePhoto = '$profilePhoto'
					   WHERE username = '$username'
					   ";
					   
	mysqli_query($conn, $updateDatabase) or die (mysqli_error($conn));
	
	//send new user all threads page after time delay (so they can see successful message)
	echo"Welcome, $username! You successfully uploaded your profile photo. You are now being forwarded to the threads page.<br />
	<a href=\"imgUpload.php\">Or click here to go to the threads page</a>";
	?>
	
<?php
	header( "refresh:5;url=imgUpload.php" );
	echo getFooter();
?>