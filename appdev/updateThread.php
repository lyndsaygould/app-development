<?php 
	require_once('functions.php');
	echo makeHeader("Update Thread");
?>

	<div id="update-thread" data-role="page">
	<div data-role="header"><h1>Update Thread</h1></div>
	<div data-role="content">
	
<?php
	//connect to database
	$conn = getConnection();
			  if ($conn === false) {			
				 echo "<p>Connection failed:".mysqli_connect_error()."</p>\n";		
			  }
			  
	//get data/info from form
	$threadID = $_POST['threadID'];
	$userID = $_POST['userID'];
	$threadTitle = $_POST['threadTitle'];
	$threadText = $_POST['threadText'];
		
	//if there is a session
	//only update if user is actually logged in
	if (isset($_SESSION['login'])) {
			
		//check the user posted the thread
		$checkUser = "SELECT userID, threadID
					  FROM ma_thread
					  WHERE userID = '$userID' AND threadID = '$threadID'
					  ";
					  
		$checkUserQuery = mysqli_query($conn, $checkUser) or die (mysqli_error($conn));
		
		//if there is an image, upload it
	if (!empty($_FILES['imgUpload']['name'])) {
		//upload image to the ftp folder
		//set the directory the images are to be saved in
		$target_dir = "uploads/";

		//specify the path of the file that will be uploaded
		//imgUpload taken from the form
		$target_file = $target_dir . basename($_FILES["imgUpload"]["name"]);

		//default upload to 1, this means true. this will be used later. if changed to 0 the file will not upload
		$upload = 1;

		//take the extention of the file that is being uploaded
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

		//make an array for errors
		$errors = array();
		
		//check to see if file already exists in file
		if (file_exists($target_file)) {
			$errors[] = "File already exists";
			$upload = 0;
		}

		//check file size
		if ($_FILES["imgUpload"]["size"] > 1000000) {
			$errors[] = "File is too large";
			$upload = 0;
		}

		//check to see if the format extention is ok and a valid image type
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
			$errors[] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed";
			$upload = 0;
		}
		//check to see if $upload has been set false by an error
		if ($upload == 0) {
			//don't upload
		//if not, upload the file
		} else {
			if (move_uploaded_file($_FILES["imgUpload"]["tmp_name"], $target_file)) {
				//upload!
			} else { //if there is some sort of error when uploadng the file echo an error message
				// Sorry, there was an error uploading your file
			}
		}
		} 
		
		//make file
	$imageName = $_FILES["imgUpload"]["name"];
	if ($imageName == '') {
		$threadImage = '';
	} else {
		$threadImage = "uploads/" . $imageName;
	}
		
		//if there was a return from the query - the user did post the thread, therefore it can update
			if (mysqli_num_rows($checkUserQuery) != 0){	
				if ($threadImage == ''){
			$updateThread = "UPDATE ma_thread
					  SET threadTitle = '$threadTitle', threadText = '$threadText'
					  WHERE threadID = '$threadID'
					  ";
				} else {
					$updateThread = "UPDATE ma_thread
					  SET threadTitle = '$threadTitle', threadText = '$threadText', threadImage = '$threadImage'
					  WHERE threadID = '$threadID'";
				}
			
					  
			mysqli_query($conn, $updateThread) or die (mysqli_error($conn));	
			header( "refresh:5;url=viewAllSelected.php?threadID=$threadID" );			
				echo"You successfully updated the thread, you will be forwarded to the updated thread in 5 seconds.";
			} else { //else, the user did not post the thread therefore can not edit it
			echo "you are not the user who posted this thread!";
			}
	}
	echo getFooter();
	?>