<?php
	require_once('functions.php');
	echo makeHeader("Login Form");
?>

<div id="login" data-role="page">
	<div data-role="content">
		<div id="center-login">
		<div id="home-logo"><img src="logo.png"></div>
	<?php 
		if (isset ($_SESSION['uName'])) {
			$username = $_SESSION['uName'];
			echo"<p class=\"center\">$username, you have successfully logged in!</p><p class=\"center\"><a href=\"logout.php\">Logout?</a></p>";
		} else {
			echo"
				<div class=\"login-form\">
						<form id=\"newUser\" method=\"post\" action=\"logonProcess.php\" data-ajax=\"false\">
							<label for=\"userName\" class=\"ui-hidden-accessible\">Username:</label>
							<input type=\"text\" name=\"userName\" id=\"userName\" placeholder=\"Username\">
							
							<label for=\"password\" class=\"ui-hidden-accessible\">Password:</label>
							<input type=\"password\" name=\"password\" id=\"password\" placeholder=\"Password\">
							
							<input type=\"submit\" value=\"Logon\">
						</form>
						
						<p class=\"center\">Don't have an account? <a href=\"newUser.php\">Register</a></p>
						<p class=\"center\"><a href=\"imgUpload.php\">Continue as a guest?</a></p>
						<p class=\"center small\">By using the application you agree to the terms and conditions</p>
				</div>
			</div><!--end center-login-->
			";
		}
	echo getFooter();
	?>
	