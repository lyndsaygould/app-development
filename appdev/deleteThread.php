<?php
	require_once('functions.php');
	echo makeHeader("Delete Thread");
?>

	<div id="delete-thread" data-role="page">
	<div data-role="header"><h1>Delete Thread</h1></div>
			
	<div data-role="content">
	
<?php	
	//when the user clicks delete thread on the thread page this code:
	//checks to see if the user is logged in and if they are the one who posted the thread they wish to delete
	//deletes the thread
	//deletes any comments that were attached to the thread
	//refers user back to main page (where all threads are listed
	//if a user somehow manages to try and delete a thread they did not post themselves, an error is shown and user is referred back to thread
	
	//connect to database
	$conn = getConnection();
			  if ($conn === false) {			
				 echo "<p>Connection failed:".mysqli_connect_error()."</p>\n";		
			  }
			  
	//get data/info from url
	$threadID = $_GET['threadID'];
		
	//only delete if user is actually logged in
	//get userID of user who is logged in
	//if there is a session
	
		if (isset($_SESSION['login'])) {
			//get username
			$username = $_SESSION['uName'];
			
			$findUserID = "SELECT userID
						FROM ma_user
						WHERE ma_user.username = '$username'
						";

			//perform query to get userID from the database
			//this is to use in form to send to next page (to update database)
			
			$queryUserID = mysqli_query($conn, $findUserID) or die(mysqli_error($conn));
			while ($row = mysqli_fetch_assoc($queryUserID)) {
				$userID = $row['userID'];
			}
			
		//check the user posted the thread
		$checkUser = "SELECT userID, threadID
					  FROM ma_thread
					  WHERE userID = '$userID' AND threadID = '$threadID'
					  ";
					  
		$checkUserQuery = mysqli_query($conn, $checkUser) or die (mysqli_error($conn));		
		
		//if there was a return from the query - the user did post the thread, therefore they can delete it
			if (mysqli_num_rows($checkUserQuery) != 0){	
			
			$deleteThread = "DELETE FROM ma_thread
					  WHERE userID = '$userID' AND threadID = '$threadID'
					  ";
					  
			mysqli_query($conn, $deleteThread) or die (mysqli_error($conn));	
			
			//delete all comments associated with thread too (no point keeping them in the database)
			
			$deleteComments = "DELETE FROM ma_comment
					  WHERE threadID = '$threadID'
					  ";
					  
			$deleteQuery = mysqli_query($conn, $deleteComments) or die (mysqli_error($conn));	
			}
			
			//if successfully deleted from database, refer back to main page
			if ($deleteQuery) {
				echo"You successfully deleted your post. <a href=\"imgUpload.php\">Go back to view all threads?</a>";
				
			} else { //else, the user did not post the thread therefore can not delete it, refer back to thread
			header( "refresh:5;url=viewAllSelected.php?threadID=$threadID" );
			echo "you are not the user who posted this thread, therefore you cannot delete it!<br />
			you will be redirected back to the thread in 5 seconds";
			}
		
	}
	
	echo getFooter();
?>