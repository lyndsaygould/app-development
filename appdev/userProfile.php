<?php
	require_once('functions.php');
	echo makeHeader("User Profile");
	
	//get userID from URL
	$userID = $_GET['userID'];
	//connect to database
	$conn = getConnection();
			  if ($conn === false) {			
				 echo "<p>Connection failed:".mysqli_connect_error()."</p>\n";		
			  }
	//for thread post		  
	$sql = "SELECT *
			FROM ma_user
			WHERE ma_user.userID = $userID
			";

		//perform query on the database
		$userQuery = mysqli_query($conn, $sql) or die(mysqli_error($conn));
		
		echo"<div id=\"user-profile\" data-role=\"page\">
		<div data-role=\"header\"><div id=\"page-logo\"><img src=\"logo.png\" alt=\"logo\"></div></div>
				
		<div data-role=\"content\">";
			
		while ($row = mysqli_fetch_assoc($userQuery)) {
			$username = $row['username'];
			$profilePhoto = $row['profilePhoto'];
			
			echo"
			<div class=\"user-profile\"><img src=\"$profilePhoto\"><br />
			$username
			</div>
			
			";
		}
		
	mysqli_free_result($userQuery);
	
	//if there is a session
				if (isset($_SESSION['login'])) {
					//get username
					$currentUser = $_SESSION['uName'];
					}
				//if the logged in username matches the user who posted the thread	
				if ($username == $currentUser) {
					echo"<p>Your posts:</p>";
				} else {
					echo"<p>Posts by $username:</p>";
				}
	
		//for thread post		  
	$userPosts = "SELECT *
			FROM ma_thread
			JOIN ma_user
			ON ma_user.userID = ma_thread.userID
			WHERE ma_thread.userID = $userID
			";

		//perform query on the database
		$getPosts = mysqli_query($conn, $userPosts) or die(mysqli_error($conn));
			
			if (mysqli_num_rows($getPosts) == 0) {
				echo"<p>There are no posts to show.</p>";
			} else {
		while ($row = mysqli_fetch_assoc($getPosts)) {
			
		$username = $row['username'];
		$profilePhoto = $row['profilePhoto'];
		$threadText = $row['threadText'];
		$threadID = $row['threadID'];
		$threadImage = $row['threadImage'];
		$threadTitle = $row['threadTitle'];
		$faveCount = $row['faveCount'];
		
			echo"
			<div class=\"single-post\">
			<div class=\"profile-photo\">
			<img src=\"$profilePhoto\" alt=\"profile photo\">
			</div>
			<div class=\"single-post-info\">
			<h2 class=\"username\">$username</h2>
			<a href=\"viewAllSelected.php?threadID=$threadID\"><h2 class=\"title\">$threadTitle</h2></a></div>
			<div class=\"single-post-content\">
		";
		
		
				if ($threadText !== '') {
			echo"<p>$threadText</p>";
		}
		
		if ($threadImage !== '') {
			echo"<img src=\"$threadImage\" alt=\"thread image\">";
		}
			
			echo"
			</p>
			<a href=\"viewAllSelected.php?threadID=$threadID\" class=\"small\">View entire post</a>
			";
			
			//if there is a session
				if (isset($_SESSION['login'])) {
					//get username
					$currentUser = $_SESSION['uName'];
					}
				//if the logged in username matches the user who posted the thread	
				if ($username == $currentUser) {
					//display edit and delete links
					echo "
					<div class=\"edit-delete\"><a href=\"editThread.php?threadID=$threadID\"><i class=\"material-icons\">mode_edit</i></a>
					<a href=\"deleteThread.php?threadID=$threadID\" onClick=\"return confirm('Are you sure you want to delete this thread?')\"><i class=\"material-icons\">delete</i></a>
					</div>
					";
				} 
				echo"
			</div><!-- end content -->
			<div class=\"clear\"></div>
		</div><!--end single-post -->
		";
			}
		}
		
	
	mysqli_close($conn);
	
	echo getFooter();
?>