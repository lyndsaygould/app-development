<?php
	require_once('functions.php');
	echo makeHeader("Login Form");
?>

	<div id="login" data-role="page">
	<div data-role="header"><h1>Bookworm</h1></div>
			
	<div data-role="content">
		
	<?php 
		if (isset ($_SESSION['uName'])) {
			$username = $_SESSION['uName'];
			echo"<p>$username, you have successfully logged in!</p><p><a href=\"logout.php\">Logout?</a></p>";
		} else {
			echo"
				<p>Use your username and password to login.</p>
				
					<form id=\"newUser\" method=\"post\" action=\"logonProcess.php\" data-ajax=\"false\">
						<label for=\"userName\">Username:</label>
						<input type=\"text\" name=\"userName\" id=\"userName\">
						
						<label for=\"password\">Password:</label>
						<input type=\"password\" name=\"password\" id=\"password\">
						
						<input type=\"submit\" value=\"Logon\">
					</form>
			";
		}
	
		echo getFooter();
	?>
	
	