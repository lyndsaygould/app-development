<?php 
	ini_set("session.save_path", "/home/unn_w14035880/sessionData");
	session_start();
	
	require_once('functions.php');
	
	//connect to database
	$conn = getConnection();
			  if ($conn === false) {			
				 echo "<p>Connection failed:".mysqli_connect_error()."</p>\n";		
			  }
			  
	//retrieve from form
	$email = $_POST['email'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	$confirmPass = $_POST['confirmPassword'];

	//hash the password for database
	$hash = password_hash($password, PASSWORD_DEFAULT);
	
	//if the username already exists
	$findUsername = "SELECT username
					FROM ma_user
					WHERE ma_user.username = '$username'
					";
					
	$usernameQuery = mysqli_query($conn, $findUsername) or die (mysqli_error($conn));
	
	$errors = array();
	//if there is a username that exists
	//(if something was returned in the query)
	if (mysqli_num_rows($usernameQuery) != 0){
		  $errors[] = "Username already exists";
	}
	
	else if (empty($email)) {
		$errors[] = "You did not fill in your email address";
	}
	
	
	else if (strlen($email) > 100) {
		$errors[] = "Your email address is too long";
	}
	
	else if (empty($username)) {
		$errors[] = "You did not fill in the username";
	}

	else if (strlen($username) > 20) {
		$errors[] = "Username must be less than 20 characters";
	}
	
	else if (strlen($username) < 6) {
		$errors[] = "Username must be more than 6 characters";
	}

	else if (empty($password)) {
		$errors[] = "You did not fill in the password";
	}
	
	else if (empty($confirmPass)) {
		$errors[] = "You need to confirm your password";
	}
	
	else if ($password !== $confirmPass) {
		$errors[] = "Your confirmation did not match the password";
	}
	
	else if (strlen($password) < 6) {
		$errors[] = "Password must be more than 6 characters";
	}

	else if (strlen($password) > 20) {
		$errors[] = "Password must be less than 20 characters";
	}
	
	if (!empty($errors)) {
		$_SESSION['errors'] = $errors;
		
		$referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'index.php';
		header("Location: $referrer");
	}
		
	//if successful
	else {
	
	//update database
	$updateDatabase = "INSERT INTO ma_user (email, username, password, passwordHash)
					   values('$email', '$username', '$password', '$hash') 
					   ";
					   
	mysqli_query($conn, $updateDatabase) or die (mysqli_error($conn));
	
	//log user in with new username
	$_SESSION['uName'] = $username;
	$_SESSION['login'] = true;
				
	//send user to upload a profile picture (done seperately for ease of use and aesthetic reasons
	header("Location: profileForm.php");
	}
?>