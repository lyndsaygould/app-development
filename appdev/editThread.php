<?php 
	require_once('functions.php');
	echo makeHeader("Edit Thread");
	
	//connect to database
	$conn = getConnection();
			  if ($conn === false) {			
				 echo "<p>Connection failed:".mysqli_connect_error()."</p>\n";		
			  }
			  
	//get threadID from URL
	$threadID = $_GET['threadID'];
	
	//get userID of user who is logged in
	//if there is a session
		if (isset($_SESSION['login'])) {
			//get username
			$username = $_SESSION['uName'];
			
			$findUserID = "SELECT userID
						FROM ma_user
						WHERE ma_user.username = '$username'
						";

			//perform query to get userID from the database
			//this is to use in form to send to next page (to update database)
			
			$queryUserID = mysqli_query($conn, $findUserID) or die(mysqli_error($conn));
			while ($row = mysqli_fetch_assoc($queryUserID)) {
				$userID = $row['userID'];
			}
			
			//query database for original thread info
			$threadSQL = "SELECT *
						 FROM ma_thread
						 WHERE ma_thread.threadID = '$threadID'
						 ";
						 
			$threadQuery = mysqli_query($conn, $threadSQL) or die(mysqli_error($conn));
			while ($row = mysqli_fetch_assoc($threadQuery)) {
				$threadTitle = $row['threadTitle'];
				$threadText= $row['threadText'];

			
			echo"
			<div id=\"all-threads\" data-role=\"page\">
			<div data-role=\"header\"><h1>Edit $threadTitle</h1></div>
			
			<div data-role=\"content\">
			<form id=\"editThread\" action=\"updateThread.php\" method=\"post\" data-ajax=\"false\" onsubmit=\"return confirm('Are you sure you want to make these changes to your thread?');\" enctype=\"multipart/form-data\">
				<input type=\"hidden\" name=\"userID\" value=\"$userID\" id=\"userID\" required/> 
				<input type=\"hidden\" name=\"threadID\" value=\"$threadID\" id=\"threadID\" required/> 
				<label for=\"threadTitle\"> Thread Title: <input type=\"text\" name=\"threadTitle\" value=\"$threadTitle\" id=\"threadTitle\" required/>
				<label for=\"imgUpload\">Upload Image:</label> <input type=\"file\" name=\"imgUpload\" id=\"imgUpload\">
				<label for=\"threadText\"> Thread Text: <input type=\"text\" name=\"threadText\" value=\"$threadText\" id=\"threadText\" required/>
				<input type=\"submit\" value=\"Edit Thread\" />
			</form>
			";
			
			}
		}
		echo getFooter();
?>