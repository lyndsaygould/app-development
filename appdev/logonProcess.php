<?php
	ini_set("session.save_path", "/home/unn_w14035880/sessionData");
	session_start();
	
	require_once('functions.php');

	$username = filter_has_var(INPUT_POST, 'userName') ? $_POST['userName']: null;
	$password  = filter_has_var(INPUT_POST, 'password') ? $_POST['password']: null;

	$username = trim($username);
	$password = trim($password);

	$errors = array();
	if (empty($username)) {
		$errors[] = "You did not fill in the username";
	}

	if (strlen($username) > 50) {
		$errors[] = "Username must be less than 50 characters";
	}

	if (empty($password)) {
		$errors[] = "You did not fill in the password";
	}

	if (strlen($password) > 225) {
		$errors[] = "Password is too long";
	}

	else {
		//connect to the database
		//if error, echo error
		$conn = getConnection();
			  if ($conn === false) {			
				 echo "<p>Connection failed:".mysqli_connect_error()."</p>\n";		
			  }
		
		//check if username exists in the table (to show an error if not
		$usernameSQL = "SELECT username FROM ma_user WHERE username = '$username'";
		
		$usernameQuery = mysqli_query($conn, $usernameSQL) or die (mysqli_error($conn));			  
			
		//if there was a return from the query (if record in database - user is trying to favourite their own post)
		if (mysqli_num_rows($usernameQuery) != 0){	
			$errors[] = "<p>Username or password incorrect, please go back and try again</p>";
		} 
		//query the users database table to get the password hash for the username entered by the user in the logon form

		$passwordSQL = "SELECT passwordHash FROM ma_user WHERE username = ?";

		$checkHash = mysqli_prepare($conn, $passwordSQL);    // prepare the sql statement

		//bind the $username entered by the user to the prepared statement. The “s” part indicates string

		mysqli_stmt_bind_param($checkHash, "s", $username);

		mysqli_stmt_execute($checkHash);    // execute the query

		//get the password hash from the query results for the given username and store it in the variable indicated

		mysqli_stmt_bind_result($checkHash, $passwordHash);

		if (mysqli_stmt_fetch($checkHash)) {
			if (password_verify($password, $passwordHash)) {
				$_SESSION['uName'] = $username;
				$_SESSION['login'] = true;
				
				//return back to previous page
				$referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'index.php';
				header("Location: $referrer");
			} 
		} else {
			$errors[] = "<p>You entered the wrong username or password, please go back to try again</p>";
		}
		
		
		if (!empty($errors)) {
			echo "<p>Please fix the following problem(s):</p>\n";

			for ($a=0; $a < count($errors); $a++) {
				echo "$errors[$a] <br />\n";

			}
		} 
	}

    mysqli_stmt_close($checkHash);
    mysqli_close($conn);
?>