<?php 
	//the functionality of this page:
	//blocks a user from favouriting their own post
	//updates the favourite count +1 when the star is clicked
	//adds a record in the database to say the user has favourited the thread
	//updates the favourite count -1 when the star is unclicked again (unfavourited)
	//removes database record to say user has no longer favourited the thread
	//returns the favourite count to be displayed to the user on other page 
	ini_set("session.save_path", "/home/unn_w14035880/sessionData");
	session_start(); // checks the status of the session
	
	require_once('functions.php');
	
	
	//connect to database
	$conn = getConnection();
			  if ($conn === false) {			
				 echo "<p>Connection failed:".mysqli_connect_error()."</p>\n";		
			  }
	
	
	if(isset($_POST['uid'])){
		echo updateDatabase($conn);
	}
	
	if (isset($_REQUEST['useJSON'])) {
		echo updateFave($conn);
	}
	
	function updateDatabase($conn) {		  
		//retrieve from form
		$threadID = $_POST['uid'];
		
		//see if a user has already favourited
		//if there is a session
		if (isset($_SESSION['login'])) {
			//get username
			$username = $_SESSION['uName'];
			
			//get userID of username to add to favourite database, so we can query if a user has already favourited
			//or if they are trying to favourite their own post
			
			$findUserID = "SELECT userID
						FROM ma_user
						WHERE ma_user.username = '$username'
						";

			//perform query to get userID from the database
			$queryUserID = mysqli_query($conn, $findUserID) or die(mysqli_error($conn));
			while ($row = mysqli_fetch_assoc($queryUserID)) {
				$userID = $row['userID'];
			}
			
			//block user from favouriting their own post
			$blockFave = "SELECT userID, threadID
					  FROM ma_thread
					  WHERE userID = '$userID' AND threadID = '$threadID'
					  ";
			$blockFaveQuery = mysqli_query($conn, $blockFave) or die (mysqli_error($conn));			  
			
			//if there was a return from the query (if record in database - user is trying to favourite their own post)
			if (mysqli_num_rows($blockFaveQuery) != 0){	
			
				//don't do anything!!!!
				
			} else { //do all other checks
						  
			//query database to see if user has already favourited this thread
		//use username from session and threadID passed in by previous page
		$checkFave = "SELECT userID, threadID
					  FROM ma_fave
					  WHERE userID = '$userID' AND threadID = '$threadID'
					  ";
					  
		$checkFaveQuery = mysqli_query($conn, $checkFave) or die (mysqli_error($conn));	
		
		//if there was a return from the query (if record in database - user has already favourited)
		if (mysqli_num_rows($checkFaveQuery) != 0){
			 //-1 favourite from database(unfavourite post)
			$unfave = "UPDATE ma_thread
							   SET faveCount = faveCount - 1
							   WHERE threadID = $threadID
							   ";
							   
			mysqli_query($conn, $unfave) or die (mysqli_error($conn));		
			
			//remove record from database incase user decides to refave later
			$removeFave = "DELETE FROM ma_fave
					  WHERE userID = '$userID' AND threadID = '$threadID'
					  ";
					  
			mysqli_query($conn, $removeFave) or die (mysqli_error($conn));	
			
		} else {	  //no record in database (meaning user hasn't favourited)
			//update database
			$updateDatabase = "UPDATE ma_thread
							   SET faveCount = faveCount + 1
							   WHERE threadID = $threadID
							   ";
							   
			mysqli_query($conn, $updateDatabase) or die (mysqli_error($conn));		
			
		//add to favourites database, to use later in preventing a user from favouriting twice
		$addFave = "INSERT INTO ma_fave (threadID, userID)
						   values('$threadID', '$userID') 
						   ";
						   
		mysqli_query($conn, $addFave) or die (mysqli_error($conn));		
		}		
		}
		}		
	}
	
	function updateFave($conn) {
			
	$threadID = $_GET['uid'];
			
	$newFaveCount = "SELECT threadID, faveCount
					 FROM ma_thread
					 WHERE threadID = $threadID
					 ";
					 
	$query = mysqli_query($conn, $newFaveCount) or die (mysqli_error($conn));
	
	$offer = mysqli_fetch_assoc($query);
    return json_encode($offer);
	}
?>