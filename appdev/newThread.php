<?php
	require_once('functions.php');
	echo makeHeader("New Thread");
	
	//connect to database
	$conn = getConnection();
			  if ($conn === false) {			
				 echo "<p>Connection failed:".mysqli_connect_error()."</p>\n";		
			  }
	
	echo"
		<div id=\"user-profile-photo\" data-role=\"page\">
			<div data-role=\"header\"><div id=\"page-logo\"><img src=\"logo.png\" alt=\"logo\"></div></div>
				
			<div data-role=\"content\">
	";
	
	//if there is a session
	if (isset($_SESSION['login'])) {
		//get username
		$username = $_SESSION['uName'];
		
		//get userID of username to add to the form to pass into the next page (to add to the database)
		
		$findUserID = "SELECT userID
					FROM ma_user
					WHERE ma_user.username = '$username'
					";

		//perform query to get userID from the database
		$queryUserID = mysqli_query($conn, $findUserID) or die(mysqli_error($conn));
		while ($row = mysqli_fetch_assoc($queryUserID)) {
				$userID = $row['userID'];
			}
			
			echo"
			<form id=\"addThread\" action=\"addNewThread.php\" method=\"post\" data-ajax=\"false\" enctype=\"multipart/form-data\">
			
				<input type=\"hidden\" name=\"userID\" value=\"$userID\" id=\"userID\" required/>
				<label for=\"threadTitle\">Post Title:</label> <input type=\"text\" name=\"threadTitle\" id=\"threadTitle\" required/>
				<label for=\"newThread\">Text:</label> <input type=\"text\" name=\"newThread\" id=\"newThread\"/>
				<label for=\"imgUpload\">Upload Image:</label> <input type=\"file\" name=\"imgUpload\" id=\"imgUpload\">
				<input type=\"submit\" value=\"Add Post\" />
				
			</form>
			";
			
			} else {
				echo "<p>If you'd like to post a thread, please login</p>";
			}
			
			//if the user tried to add a thread but there is some errors
			if (!empty($_SESSION['threadErrors'])) { 
				$errors = $_SESSION['threadErrors'];
				
				for ($a = 0; $a < count($errors); $a++) {
					echo "$errors[$a] <br />\n";
				}
			}
			
			echo getFooter();
			?>