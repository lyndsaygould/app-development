<?php
	require_once('functions.php');
	echo makeHeader("New User");
?>

	<div id="new-user" data-role="page">
		<div data-role="content">
			<div id="center-login">
				<div id="home-logo"><img src="logo.png"></div>
	
		<?php
			if (isset ($_SESSION['uName'])) {
						$username = $_SESSION['uName'];
						echo"<p>$username, you have successfully logged in!</p><p><a href=\"logout.php\">Logout?</a></p>";
					} else {
				
			echo"
				<form id=\"newUser\" action=\"addUser.php\" method=\"post\" data-ajax=\"false\" enctype=\"multipart/form-data\">
					<label for=\"email-address\">Email Address:</label> <input type=\"email\" name=\"email\" id=\"email\" required/>
					<label for=\"username\">Username:</label> <input type=\"text\" name=\"username\" id=\"username\" required/>  
					<label for=\"password\">Password:</label> <input type=\"password\" name=\"password\" id=\"password\" required/>
					<label for=\"confirmPassword\">Confirm password:</label> <input type=\"password\" name=\"confirmPassword\" id=\"confirmPassword\" required/>
					<input type=\"submit\" value=\"Sign Up\" />
				</form>
				
				<p class=\"center small\">By using the application you agree to the terms and conditions</p>
				<p class=\"center\"> <a href=\"imgUpload.php\">Continue as a guest?</a></p>
			";
					
				//if the user tried to login but there is some errors
				if (!empty($_SESSION['errors'])) { 
					$errors = $_SESSION['errors'];
					
					for ($a = 0; $a < count($errors); $a++) {
						echo "$errors[$a] <br />\n";
					}
				}
			}
		?>
		</div><!-- end center-login-->
	</div><!-- end content-->	
</div><!-- end page-->	