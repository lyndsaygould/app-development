<?php 
	require_once('functions.php');
	
	//connect to database
	$conn = getConnection();
			  if ($conn === false) {			
				 echo "<p>Connection failed:".mysqli_connect_error()."</p>\n";		
			  }
			  
	//retrieve from form
	
	$threadID = isset($_REQUEST['threadID']) ? $_REQUEST['threadID'] : null;
	$userID = isset($_REQUEST['userID']) ? $_REQUEST['userID'] : null;
	$newComment = isset($_REQUEST['newComment']) ? $_REQUEST['newComment'] : null;
	
	$newComment = trim($newComment);
	
	$newComment = filter_var($newComment, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
	$newComment = filter_var($newComment, FILTER_SANITIZE_SPECIAL_CHARS);

	//update database
	$updateDatabase = "INSERT INTO ma_comment (threadID, userID, comment)
					   values('$threadID', '$userID', '$newComment') 
					   ";
					   
	mysqli_query($conn, $updateDatabase) or die (mysqli_error($conn));
	
	//redirect back to comment page
	$referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'index.php';
				header("Location: $referrer");
?>