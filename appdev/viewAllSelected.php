<?php
	require_once('functions.php');
	echo makeHeader("View All");
?>

	<div id="view-all" data-role="page">
	<div data-role="header"><div id="page-logo"><img src="logo.png" alt="logo"></div></div>
			
	<div data-role="content">
	
<?php
	
	//get threadID from URL
	$threadID = $_GET['threadID'];
	//connect to database
	$conn = getConnection();
			  if ($conn === false) {			
				 echo "<p>Connection failed:".mysqli_connect_error()."</p>\n";		
			  }
	//for thread post		  
	$sql = "SELECT *
					FROM ma_thread
					JOIN ma_user
					ON ma_user.userID = ma_thread.userID
					WHERE ma_thread.threadID = $threadID
					";

			//perform query on the database
			$eventQuery = mysqli_query($conn, $sql) or die(mysqli_error($conn));
			
			while ($row = mysqli_fetch_assoc($eventQuery)) {
				$threadID = $row['threadID'];
				$threadUsername = $row['username'];
				$profilePhoto = $row['profilePhoto'];
				$faveCount = $row['faveCount'];
				$threadTitle = $row['threadTitle'];
				$threadText = $row['threadText'];
				$threadImage = $row['threadImage'];
			
				echo"
				
				<div class=\"single-post\">
				<div class=\"profile-photo\">
				<img src=\"$profilePhoto\" alt=\"profile photo\">
				</div>
				<div class=\"single-post-info\">
				<p class=\"username\">$threadUsername</p>
				<a href=\"viewAllSelected.php?threadID=$threadID\"><h2 class=\"title\">$threadTitle</h2></a></div>
				<div class=\"single-post-content\">
				";
				
				//threads can have an image, text or both
				if ($threadText !== '') {
					echo"<p>$threadText</p>";
				}
		
				if ($threadImage !== '') {
					echo"<img src=\"$threadImage\" alt=\"thread image\">";
				}
				
				//display favourite count
				//if there is a session
				if (isset($_SESSION['login'])) {
					//get username
					$username = $_SESSION['uName'];
					//echo the favourites with ids that have an onclick attached so the logged in user can favourite post
					echo"
						<div class=\"count\" data-id=\"$threadID\">
						<p class=\"dk\">&#9733; $faveCount favourites</p></div>
					";
					//echo the favourites in a way that displays the number but does not allow a not logged in user to click
					} else {
						$username = null;
						echo"<p>&#9733; $faveCount favourites</p>";
					}
				//if the logged in username matches the user who posted the thread	
				if ($threadUsername == $username) {
					//display edit and delete links
					echo "
					<div class=\"edit-delete\"><a href=\"editThread.php?threadID=$threadID\"><i class=\"material-icons\">mode_edit</i></a>
					<a href=\"deleteThread.php?threadID=$threadID\" onClick=\"return confirm('Are you sure you want to delete this thread?')\"><i class=\"material-icons\">delete</i></a>
					</div>
					";
				} 
			}
			echo"</div></div>";
			mysqli_free_result($eventQuery);
			
			//for comments
			
			$sqlComments = "SELECT *
					FROM ma_comment
					JOIN ma_thread
					ON ma_comment.threadID = ma_thread.threadID
					JOIN ma_user
					ON ma_user.userID = ma_comment.userID
					WHERE ma_comment.threadID = $threadID
					ORDER BY ma_comment.timeStamp
					"; //comments display oldest first, incase there is some sort of discussion going on

			//perform query on the database
			$commentQuery = mysqli_query($conn, $sqlComments) or die(mysqli_error($conn));
			$commentNumber = mysqli_num_rows($commentQuery);
			
			echo"<h3>$commentNumber comments</h3>";
			
			while ($row = mysqli_fetch_assoc($commentQuery)) {
			$threadID = $row['threadID'];
			$countID = $row['threadID'];
			$commentUsername = $row['username'];
			$comment = $row['comment'];
			$faveCount = $row['faveCount'];
			$commentProfile = $row['profilePhoto'];

			echo"
				<div class=\"comment-profile\">
				<img src=\"$commentProfile\" alt=\"profile picture\">
				</div>
				
				<div class=\"comment-text\">
				<strong>$commentUsername</strong><br />
				$comment<br />
				</div>
			
			<div class=\"clear\"></div><br />	
			 ";
			}
			mysqli_free_result($commentQuery);
			
			//if there is a session
			if (isset($_SESSION['login'])) {
				//get username
				$username = $_SESSION['uName'];
				
				//get userID of username to add to the form to pass into the next page (to add to the database)
				
				$findUserID = "SELECT userID, profilePhoto
							FROM ma_user
							WHERE ma_user.username = '$username'
							";

				//perform query to get userID from the database
				$queryUserID = mysqli_query($conn, $findUserID) or die(mysqli_error($conn));
				while ($row = mysqli_fetch_assoc($queryUserID)) {
					$userID = $row['userID'];
					$userProfile = $row['profilePhoto'];
				}
				echo"
				

					<form id=\"addComment\" action=\"addNewComment.php\" method=\"post\"><br />
						<input type=\"hidden\" name=\"threadID\" value=\"$threadID\" id=\"threadID\" required/>
						<input type=\"hidden\" name=\"userID\" value=\"$userID\" id=\"userID\" required/>
						
						<label for=\"newComment\" class=\"ui-hidden-accessible\">Comment:</label>
						<input type=\"text\" name=\"newComment\" id=\"newComment\" placeholder=\"Write a comment...\" required/>
						<input type=\"submit\" value=\"Add Comment\" />
					</form>
				";
			} else {
				echo"<p>Please login if you'd like to post a comment.</p>";
			}
	mysqli_close($conn);
?>

<script type="text/javascript">
$(document).ready(function(){
    $(".count").click(function(){
		
		$.ajax({
            url: "updateFave.php",
            type: "POST",
            data: {uid: $(this).attr('data-id')},
            success: function(data){
				
            }
        });
    });
	
	$(".count").click(function(){
	$.ajax({
           dataType: "json",
           method: "get",
		   data: {uid: $(this).attr('data-id')},
           url: "updateFave.php?useJSON"}
       )
	   //function to be called if successful
	   //data = data returned from server according to datatype
	   //status = string describing status
	   //jqxhr = xmlhttp request
       .done(function (data, status, jqxhr) {
		   console.log(data);
		  $( ".dk" ).empty();
		  $(".dk").append("&#9733; " + data.faveCount + " favourites");
		   })
           
       .fail(function(jqxhr, textStatus, errorThrown) {
                document.getElementsByClassName('count').innerHTML = "fail";
           }
       );
	});
});

</script>

<?php
echo getFooter();
?>